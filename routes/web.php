<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/users', 'ProfileController@index')->name('users.index');
//Route::get('/users/create', 'ProfileController@create')->name('users.create');
//Route::post('/users', 'ProfileController@store')->name('users.store');
//Route::get('/users/{id}/edit', 'ProfileController@edit')->name('users.edit');
//Route::get('/users/{id}', 'ProfileController@show')->name('users.show');
//Route::put('/users/{id}', 'ProfileController@update')->name('users.update'); //put/patch
//Route::delete('/users/{id}', 'ProfileController@destroy')->name('users.destroy');

Route::get('/users/filter', 'ProfileController@filter')->name('users.filter');
Route::resource('/users', 'ProfileController');


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = ['name', 'hobby', 'gender', 'bio', 'picture', 'skills', 'date_of_birth'];

    protected $dates = ['date_of_birth'];
}

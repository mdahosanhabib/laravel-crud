<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Profile;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
//        dd($request->all());

        if(isset($request->gender)){
            $users = Profile::where('gender', $request->gender)->paginate(2);
        }elseif (isset($request->sort_by_name)){
            $users = Profile::orderBy('name', $request->sort_by_name)->paginate(2);
        }else{
            $users = Profile::paginate(2);
        }

        return view('users.index', compact('users'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(UserRequest $request)
    {
        try{
            $data = $request->except('picture', 'skills', 'hobby');
            if($request->hasFile('picture')){
                $data['picture'] = $this->uploadImage($request->picture, $request->name);
            }
            $data['skills'] = serialize($request->skills);
            $data['hobby'] = serialize($request->hobby);
            Profile::create($data);
            return redirect()->route('users.index')->withStatus('Created Successfully!');
        }catch (QueryException $exception){
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }

    }

    public function edit(Profile $user)
    {
//        $user = Profile::findOrFail($id);
        return view('users.edit', compact('user'));
    }

    public function show(Profile $user)//route model binding
    {
//        $user = Profile::findOrFail($id);
//        dd($profile);
        return view('users.show', compact('user'));
    }


    public function update(Request $request, Profile $user)
    {
        try{
//            $user = Profile::findOrFail($id);
            $data = $request->except('picture', 'skills', 'hobby');
            if($request->hasFile('picture')){
                $data['picture'] = $this->uploadImage($request->picture, $request->name);
            }else{
                $data['picture'] = $user->picture;
            }
            $data['skills'] = serialize($request->skills);
            $data['hobby'] = serialize($request->hobby);
            $user->update($data);
            return redirect()->route('users.index')->withStatus('Updated Successfully !');
        }catch (QueryException $exception){
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }


    public function destroy(Profile $user)
    {
        try{
//            $user = Profile::where('id', $id)->first();
//            $user = Profile::findOrFail($id);
            $user->delete();
            return redirect()->route('users.index')->withStatus('Deleted Successfully !');
        }catch (QueryException $exception){
            return redirect()->back()->withInput()->withErrors($exception->getMessage());
        }
    }

    private function uploadImage($file, $title)
    {
        $extension = $file->getClientOriginalExtension();
        $fileName = date('y-m-d') . '_' . time() . '_' . $title . '.' . $extension;
//        $file->move(storage_path('app/public/products/'), $fileName);
        Image::make($file)->resize(300, 300)->save(storage_path('app/public/users/') . $fileName);
        return $fileName;
    }

//    public function filter(Request $request)
//    {
//        dd($request->all());
//    }
}

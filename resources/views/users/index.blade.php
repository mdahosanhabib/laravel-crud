<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Users</title>
</head>
<body>

<a href="{{ route('users.create') }}">Add New</a>
@if (session('status'))
       {{ session('status') }}
@endif
<form action="{{ route('users.index') }}">
    <select name="gender">
        <option value="">Select Gender</option>
        <option value="Male">Male</option>
        <option value="Female">Female</option>
    </select>
    <button type="submit">Filter</button>
</form>

<table border="1" cellpadding="5">
    <thead>
        <tr>
            <td>Sl</td>
            <td>Name<form action="{{ route('users.index') }}">
                    <select name="sort_by_name">
                        <option value="">Select One</option>
                        <option value="asc">Ascending</option>
                        <option value="desc">Descending</option>
                    </select>
                    <button type="submit">Filter</button>
                </form></td>
            <td>Picture</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>

    @php $sl=0 @endphp
    @foreach($users as $user)
        <tr>
            <td>{{ ++$sl }}</td>
            <td>{{ $user->name }}</td>
            <td>
                @if(file_exists(storage_path('app/public/users/').$user->picture) && (!is_null($user->picture)))
                    <img src="{{asset('storage/users/'.$user->picture)}}" width="100px"
                         height="100px">
                @else
                    <img src="{{ asset('default.png') }}" alt="Default Picture" height="100">
                @endif
            </td>
            <td>
                <a href="{{ route('users.show', $user->id) }}">Show</a> |
                <a href="{{ route('users.edit', $user->id) }}">Edit</a> |
                <form action="{{ route('users.destroy', $user->id) }}" method="post" style="display: inline;">
                    @csrf
                    @method('delete')
                    <button type="submit" onclick="return confirm('Are You Sure ?')">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $users->links() }}
</body>
</html>

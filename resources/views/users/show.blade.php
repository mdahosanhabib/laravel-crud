<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Details</title>
</head>
<body>
<table>
    <tr>
        <td>Name</td>
        <td>{{ $user->name }}</td>
    </tr>
    <tr>
        <td>Picture</td>
        <td></td>
    </tr>

    <tr>
        <td>Gender</td>
        <td>{{ $user->gender }}</td>
    </tr>

    <tr>
        <td>Date Of Birth</td>
        <td>{{ $user->date_of_birth->toFormattedDateString() }}</td>
    </tr>

    <tr>
        <td>Hobby</td>
        <td>
            <ul>
                @foreach(unserialize($user->hobby) as $hobby)
                    <li>{{ $hobby }}</li>
                @endforeach
            </ul>
        </td>
    </tr>
    <tr>
        <td>Skills</td>
        <td>
            @foreach(unserialize($user->skills) as $skill)
                <li>{{ $skill }}</li>
            @endforeach
        </td>
    </tr>
    <tr>
        <td>Bio data</td>
        <td>{!! $user->bio !!}</td>
    </tr>
</table>
</body>
</html>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit User</title>
</head>
<body>
<a href="{{ route('users.index') }}">List</a>
@if ($errors->any())
    {{--<div class="alert alert-danger">--}}
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    {{--</div>--}}
@endif
<form action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data" method="post">
    @csrf
    @method('put')
    <input type="text" name="name" placeholder="Enter name" value="{{ $user->name }}"><br>
    <input type="file" name="picture"><br>

    Date Of Birth<br>
    <input type="date" name="date_of_birth" value="{{ \Carbon\Carbon::parse($user->date_of_birth)->format('Y-m-d') }}"><br>

    Gender<br>
    <input type="radio" name="gender" value="Male"  {{ $user->gender=='Male' ? 'checked' : '' }} > Male
    <input type="radio" name="gender" value="Female" {{ $user->gender=='Female' ? 'checked' : '' }}> Female

    <br>
    Hobby<br>
    <input type="checkbox" name="hobby[]" value="Listening" {{ in_array('Listening', unserialize($user->hobby)) ? 'checked' : '' }}>Listening<br>
    <input type="checkbox" name="hobby[]" value="Reading" {{ in_array('Reading', unserialize($user->hobby)) ? 'checked' : '' }}>Reading<br>
    <input type="checkbox" name="hobby[]" value="Coding"  {{ in_array('Coding', unserialize($user->hobby)) ? 'checked' : '' }}>Coding<br>


    <select name="skills[]" multiple>
        <option value="">Select One</option>
        <option value="PHP" {{ in_array('PHP', unserialize($user->skills)) ? 'selected' : '' }}>PHP</option>
        <option value="JavaScript" {{ in_array('JavaScript', unserialize($user->skills)) ? 'selected' : '' }}>JavaScript</option>
        <option value="MySql" {{ in_array('MySql', unserialize($user->skills)) ? 'selected' : '' }}>MySql</option>
    </select>
    <br>
    <textarea name="bio" id="bioData">
        {{ $user->bio }}
    </textarea>
    <button type="submit">Save</button>

</form>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/tinymce.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/jquery.tinymce.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.0/plugins/spellchecker/plugin.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea#bioData',
        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help',
        toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tiny.cloud/css/codepen.min.css'
        ],
        image_class_list: [
            { title: 'None', value: '' },
            { title: 'Some class', value: 'class-name' }
        ],
        importcss_append: true,
        height: 400,
        file_picker_callback: function (callback, value, meta) {
            /* Provide file and text for the link dialog */
            if (meta.filetype === 'file') {
                callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
            }

            /* Provide image and alt text for the image dialog */
            if (meta.filetype === 'image') {
                callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
            }

            /* Provide alternative source and posted for the media dialog */
            if (meta.filetype === 'media') {
                callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
            }
        },
        templates: [
            { title: 'Some title 1', description: 'Some desc 1', content: 'My content' },
            { title: 'Some title 2', description: 'Some desc 2', content: '<div class="mceTmpl"><span class="cdate">cdate</span><span class="mdate">mdate</span>My content2</div>' }
        ],
        template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
        template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
        image_caption: true,

        spellchecker_dialog: true,
        spellchecker_whitelist: ['Ephox', 'Moxiecode']
    });
</script>


</body>
</html>
